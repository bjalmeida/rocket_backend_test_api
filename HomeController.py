import Service
from flask import Flask,jsonify,request


app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
apiUrl='/api/tareas'

cache = []

'''
argumentos para crear una nueva tarea
id se crea por default, la fecha de creacion es del dia del momento que se creo

desc: descripcion de la tarea
stat: estado de la tarea pendiente/completado
dur:duracion de la tarea en minutos

'''

@app.route(apiUrl+'/crear', methods=['POST'])
def crearTarea():
    global cache
    cache = Service.new(request,cache)
    return 'OK'
'''
requiere id
'''
@app.route(apiUrl+'/borrar', methods=['DELETE'])
def borrarTarea():
    global cache
    cache = Service.delete(request,cache)
    return 'OK'

'''
modifica el estado de la tarea, parametros:

stat: estado de la tarea pendiente/completado
dur:duracion de la tarea en minutos
'''
@app.route(apiUrl+'/modificar', methods=['PUT'])
def modificarTarea():
    global cache
    cache=Service.update(request,cache)
    return 'OK'


@app.route(apiUrl+'/consultarStatus', methods=['GET'])
def consultarStatus():
    global cache
    return Service.searchByStatus(request,cache)


@app.route(apiUrl+'/consultarDesc', methods=['GET'])
def searchByDescription(): 
    global cache
    return Service.searchByDescription(request,cache)


@app.route(apiUrl+'/crearLista', methods=['POST'])
def requestTweets():
    global cache 
    cache = Service.createRandomTasks(cache) 
    return 'OK'