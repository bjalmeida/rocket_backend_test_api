class Tarea:
    def __init__(self,  id, descripcion, duracion, tiempoRegistrado, status, fechaCreacion):
        self.id = id
        self.descripcion = descripcion
        self.duracion = duracion
        self.tiempoRegistrado = tiempoRegistrado
        self.status = status
        self.fechaCreacion = fechaCreacion
    
    def serialize(self):
        return {
                        
            'id' : self.id, 
            'descripcion':self.descripcion,
            'duracion':self.duracion,  
            'tiempoRegistrado':self.tiempoRegistrado,
            'status':self.status,
            'fechaCreacion':self.fechaCreacion.strftime("%Y-%m-%d %H:%M:%S")
        }