from Model import Tarea
from flask import jsonify
import datetime
import uuid
from dict2xml import dict2xml as xmlify
import random


def  new(request,listaTareas):
    
    hoy = datetime.datetime.now()
    tarea = Tarea(uuid.uuid4(),request.args.get('desc'),request.args.get('dur'),0 ,request.args.get('stat'), hoy )
    listaTareas.append(tarea)
    
    return listaTareas

def delete(request,listaTareas):
    id = request.args.get('id')
    
    for tarea in listaTareas:
        if tarea.id == id:
            listaTareas.remove(tarea)
            break        
            
    return listaTareas



def update(request,listaTareas):
    
    id = request.args.get('id')
    
    for indice,tarea in listaTareas:
        if tarea.id == id:
            listaTareas[indice].descripcion = request.args.get('stat')
            break     
    
    return listaTareas


def searchByStatus(request,listaTareas):
    
    status = request.args.get('stat')
    response=[]
    for tarea in listaTareas:
        if tarea.status == status:
            response.append(tarea)
                        
    return defineOutput(request,response)


def searchByDescription(request,listaTareas):
    
    descripcionTarea = request.args.get('q')
    response=[]
    for tarea in listaTareas:
        if descripcionTarea in tarea.descripcion:
            response.append(vars(tarea))
    
    return defineOutput(request,response)
    

def createRandomTasks(listaTareas):
    
    tareas=['crear','enviar','imprimir','revisar','quemar']
    objetos=['reporte','nomina','presentacion','correo','documentacion']
    
   
   
    for x in range(49):
        diaAnterior=random.randrange(0, 7)
        fecha = datetime.datetime.now()
        fecha = fecha.replace(day=(fecha.day-diaAnterior))
        descripcion = tareas[random.randrange(0, 4)] + ' ' + objetos[random.randrange(0, 4)]
      
        duracion = random.randrange(15, 120)
        # tiempo trancurrido desde el inicio de la tarea
        tiempoRegistrado = duracion * (random.randrange(80, 120)/100) 
        
        if tiempoRegistrado > duracion:
            tiempoRegistrado = duracion
            status='completado'
        else:
            status='pendiente'
            
        tarea = Tarea(uuid.uuid4(),descripcion,duracion, tiempoRegistrado, status, fecha )
        listaTareas.append(tarea)
       

    return listaTareas

#Devuelve JSON o XML segun el header de la peticion
def defineOutput(request,response):
        if request.headers['Accept'] == 'text/xml':
            xml=''
            for r in response:
                xml+=xmlify(vars(r), wrap="tarea", indent="  ")
                            
            return xml

        elif request.headers['Accept'] == 'application/json':
            jsonText=''
            for r in response:
                
                jsonText += str(r.serialize())
                        
            return '['+jsonText+']'
    
     